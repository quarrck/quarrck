import EventBus from 'vue';
import Conversation from '../lib/conversation';
import Message from '../lib/message';
import store from '../store';

class Asti {
	constructor() {
		this.eventBus = new EventBus();
		this.debug = false;
		this.isReady = false;
		document.addEventListener('astilectron-ready', () => {
			console.log('Finished loading astilectron...');
			this.isReady = true;
			/* eslint-disable-next-line */
			astilectron.onMessage(resp => this.emit(resp.name, resp.payload));
			this.listen('new_message', payload => {
				console.log(payload);
				store.commit('addMessage', Message.fromPayload(payload));
			});
		});
	}
	onReady(callback) {
		const delay = 100;
		if (!this.isReady) {
			setTimeout(() => {
				if (this.isReady) {
					callback();
				} else {
					this.onReady(callback);
				}
			}, delay);
		} else {
			callback();
		}
	}
	listen(name, callback, once = false) {
		if (once) {
			this.eventBus.$once(name, callback);
			return;
		}
		this.eventBus.$on(name, callback);
	}
	emit(name, payload = {}) {
		this.log(name, payload);
		this.eventBus.$emit(name, payload);
	}
	send(name, payload = {}, callback = () => {}) {
		/* eslint-disable-next-line */
		astilectron.sendMessage(
			{
				name: name,
				payload: payload,
			},
			resp => callback(resp.payload),
		);
	}
	log(...message) {
		if (this.debug) {
			console.log(message);
		}
	}
}

export default {
	install(Vue) {
		console.log('Initializing astilectron...');
		Vue.prototype.$astor = new Asti();
		Vue.mixin({
			methods: {
				sendMessage(conv, message) {
					if (message.trim() == '') {
						return;
					}
					this.$astor.send('new_message', {
						conversation: conv,
						content: this.$sanitize(message),
					});
				},
				init() {
					this.$astor.send('init', {}, payload => {
						console.log(payload);
						for (const conversation of payload.Conversations) {
							this.$store.commit(
								'addConversation',
								Conversation.fromPayload(conversation),
							);
						}
					});
				},
			},
		});
	},
};
