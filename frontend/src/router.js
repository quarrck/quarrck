import Vue from 'vue';
import VueRouter from 'vue-router';
import Conversations from './views/Conversations';
import ChooseConversation from './views/ChooseConversation';
import Conversation from './views/Conversation';
import Settings from './views/Settings';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		redirect: '/conversations',
	},
	{
		path: '/conversations',
		component: Conversations,
		children: [
			{
				path: '',
				component: ChooseConversation,
			},
			{
				path: ':conversation',
				component: Conversation,
			},
		],
	},
	{
		path: '/settings',
		component: Settings,
	},
];

export default new VueRouter({
	routes,
	mode: 'history',
});
