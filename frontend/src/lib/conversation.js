import Message from './message';

export default class Conversation {
	constructor(uuid, name, description, picture, key, server, messages) {
		this.uuid = uuid;
		this.name = name;
		this.description = description;
		this.picture = picture;
		this.key = key;
		this.server = server;
		this.messages = messages;
	}

	static fromPayload(payload) {
		return new Conversation(
			payload.UUID,
			payload.Name,
			payload.Description,
			payload.Picture,
			payload.Key,
			payload.Server,
			payload.Messages.map(msg => Message.fromPayload(msg)),
		);
	}
}
