export default class Message {
	constructor(uuid, from, topic, content, created, received, conversation) {
		this.uuid = uuid;
		this.from = from;
		this.topic = topic;
		this.content = content;
		this.created = created;
		this.received = received;
		this.conversation = conversation;
	}

	static fromPayload(payload) {
		return new Message(
			payload.UUID,
			payload.From,
			payload.Topic,
			payload.Content,
			payload.Created,
			payload.Received,
			payload.Conversation,
		);
	}
}
