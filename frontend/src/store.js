import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const initialState = {
	conversations: [],
};

export default new Vuex.Store({
	state: initialState,
	getters: {
		getConversation: state => uuid => {
			return state.conversations.find(c => c.uuid == uuid);
		},
		getConversations: state => {
			return state.conversations;
		},
	},
	mutations: {
		addConversation: (state, conv) => {
			state.conversations.push(conv);
		},
		addMessage: (state, message) => {
			state.conversations
				.find(c => c.uuid == message.conversation)
				.messages.push(message);
		},
	},
	actions: {},
});
