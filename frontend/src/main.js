import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Astor from './plugins/astor';
import VueSanitize from 'vue-sanitize';
import './quasar';

Vue.config.productionTip = false;

Vue.use(Astor);
Vue.use(VueSanitize);

new Vue({
	store,
	router,
	render: h => h(App),
}).$mount('#app');
