module.exports = {
	outputDir: '../resources/app',
	publicPath: '/',

	pluginOptions: {
		quasar: {
			importStrategy: 'kebab',
			rtlSupport: true,
		},
	},

	transpileDependencies: ['quasar'],
};
