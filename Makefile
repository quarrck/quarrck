setup_frontend:
	cd frontend && npm install

setup_backend:
	go mod vendor
	go run setup/main.go

setup: setup_frontend setup_backend
	go get -u github.com/asticode/go-astilectron-bundler/...

build_backend: build_frontend
	astilectron-bundler -ldflags="-X main.Env=prod"

build_frontend:
	cd frontend && npm run build

build: build_frontend build_backend

run_frontend:
	cd frontend && npm run serve
run_backend:
	go build -o app
	./app

run: run_frontend run_backend

compile:
	echo "Compiling for every OS and Platform"
	GOOS=linux GOARCH=arm go build -o bin/main-linux-arm .
	GOOS=linux GOARCH=arm64 go build -o bin/main-linux-arm64 .
	GOOS=freebsd GOARCH=386 go build -o bin/main-freebsd-386 .
