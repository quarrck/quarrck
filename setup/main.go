package main

import (
	"os"
	"path/filepath"

	db "gitlab.com/quarrck/quarrck/backend/repo/gorm"

	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

const UUID = "5f63c2bd-67f7-4b4d-9630-006b3df7a18c"

func main() {

	dir := "output/dev/"

	if err := os.MkdirAll(dir, 0777); err != nil {
		log.Fatalf("Error Mkdir: %v", err)
	}

	c, err := gorm.Open("sqlite3", filepath.Join(dir, "quarrck.sqlite"))
	if err != nil {
		log.Fatalf("Error open db: %v", err)
	}

	c = c.Debug()

	if err := c.AutoMigrate(&db.Config{}, &db.Server{}, &db.Contact{}, &db.Conversation{}, &db.ConversationServer{}, &db.ConversationContact{}).Error; err != nil {
		log.Fatal(err)
	}

	if err := c.Save(&db.Config{
		Key:   "UUID",
		Value: UUID,
	}).Error; err != nil {
		log.Fatal(err)
	}

	if err := c.FirstOrCreate(&db.Server{
		Type: "WS",
		URL:  "ws://localhost:8888/connect",
	}).Error; err != nil {
		log.Fatal(err)
	}

	if err := c.FirstOrCreate(&db.Contact{
		UUID: UUID,
	}).Error; err != nil {
		log.Fatal(err)
	}

	if err := c.FirstOrCreate(&db.Contact{
		UUID: "3ba8ebc6-d54c-4fde-bc76-9071f811a17b",
	}).Error; err != nil {
		log.Fatal(err)
	}

	if err := c.FirstOrCreate(&db.Conversation{
		UUID:   "c13b2ac4-85d3-45eb-ace9-8609330ba740",
		Name:   "Test1",
		Key:    "821bd32245ea13827f26e64e684ce64c407eb464bf9a5874494e7347ac370bf6",
		Status: "active",
		Server: []db.ConversationServer{
			{ServerID: 1},
		},
		Contacts: []db.ConversationContact{
			{ContactUUID: "3ba8ebc6-d54c-4fde-bc76-9071f811a17b"},
		},
	}).Error; err != nil {
		log.Fatal(err)
	}

	if err := c.FirstOrCreate(&db.Conversation{
		UUID:   "832fa9c6-7ed9-459f-b261-3b1fd9b21293",
		Name:   "Test2",
		Key:    "821bd32245ea13827f26e64e684ce64c407eb464bf9a5874494e7347ac370bf6",
		Status: "active",
		Server: []db.ConversationServer{
			{ServerID: 1},
		},
		Contacts: []db.ConversationContact{
			{ContactUUID: "3ba8ebc6-d54c-4fde-bc76-9071f811a17b"},
		},
	}).Error; err != nil {
		log.Fatal(err)
	}

}
