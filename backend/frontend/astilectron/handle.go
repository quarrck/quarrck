package frontend

import (
	"fmt"

	"github.com/asticode/go-astilectron"
	bootstrap "github.com/asticode/go-astilectron-bootstrap"
	"gitlab.com/quarrck/quarrck/backend/domain"
)

const messageLimit = 100

type Init struct {
	UUID          string
	Conversations []InitConversation
}

func (f *frontend) HandleMessages(w *astilectron.Window, m bootstrap.MessageIn) (payload interface{}, err error) {
	switch m.Name {
	case "init":
		conversations, err := f.getConversationsForInit()
		if err != nil {
			return nil, err
		}
		return Init{
			UUID:          f.d.User().UUID(),
			Conversations: conversations,
		}, nil
	case "new_message":
		return f.onNewMessage(m.Payload)

	case "get_messages":
		return f.onGetMessages(m.Payload)

	case "new_conversation":
		return f.onNewConversation(m.Payload)

	case "get_conversations":
		return f.onGetConversations()

	case "get_conversation":
		return f.onGetConversation(m.Payload)

	case "invite_conversation_contact":
		return f.onInviteConversationContacts(m.Payload)

	case "get_contacts":
		return f.d.Contact().GetContacts()

	case "get_contact":
		return f.onGetContact(m.Payload)

	case "new_contact":
		return f.onNewContact(m.Payload)

	case "new_server":
		return f.onNewServer(m.Payload)

	case "get_server":
		return f.onGetServers()

	case "get_server_types":
		return f.onGetServerTypes()

	default:
		return nil, fmt.Errorf("Unknown Message Name: %s. ", m.Name)
	}
}

func (f *frontend) isActive() bool {
	return f.w != nil
}

func (f *frontend) NewMessage(msg *domain.Message) error {
	if f.w == nil || len(f.w) < 1 {
		return nil
	}
	if err := bootstrap.SendMessage(f.w[0], "new_message", parseNewMessage(*msg)); err != nil {
		f.d.Logger().Warnf("Error sending new message to frontend: %v", err)
		return err
	}
	return nil
}
