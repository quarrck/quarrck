package frontend

import (
	"fmt"

	"github.com/asticode/go-astilectron"
	bootstrap "github.com/asticode/go-astilectron-bootstrap"
	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Frontend interface {
	OnWait(a *astilectron.Astilectron, w []*astilectron.Window, m *astilectron.Menu, t *astilectron.Tray, tm *astilectron.Menu) error
	HandleMessages(w *astilectron.Window, m bootstrap.MessageIn) (payload interface{}, err error)
}

type frontend struct {
	d  *domain.Domain
	a  *astilectron.Astilectron
	w  []*astilectron.Window
	m  *astilectron.Menu
	t  *astilectron.Tray
	tm *astilectron.Menu
}

func New(d *domain.Domain) Frontend {
	f := frontend{
		d: d,
	}
	f.d.Bus().Subscribe("new_message", func(c domain.Conversation, msg domain.Message) {
		bootstrap.SendMessage(f.w[0], "new_message", newMessage{
			Conversation: c.UUID(),
			Message:      parseMessage(msg),
		})
	})
	return &f
}

func (f *frontend) OnWait(a *astilectron.Astilectron, w []*astilectron.Window, m *astilectron.Menu, t *astilectron.Tray, tm *astilectron.Menu) error {
	f.a = a
	f.w = w
	f.m = m
	f.t = t
	f.tm = tm
	for i := range f.w {
		if f.w[i] != nil {
			f.w[i].OpenDevTools()
		}

		if err := bootstrap.SendMessage(f.w[i], "test", map[string]string{
			"msg":    "Hello",
			"window": fmt.Sprintf("%d", i),
		}); err != nil {
			f.d.Logger().Warn(err)
		}
	}
	return nil
}
