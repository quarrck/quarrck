package frontend

import (
	"encoding/json"
	"fmt"

	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Conversation struct {
	UUID        string
	Name        string
	Description string
	Picture     string
}

type InitConversation struct {
	Conversation
	Messages []Message
}

type NewConversation struct {
	Conversation
	Contacts []string
	Servers  []uint
}

func parseConversation(c domain.ConversationData) Conversation {
	return Conversation{
		UUID:        c.UUID,
		Name:        c.Name,
		Description: c.Description,
		Picture:     c.Picture,
	}
}

func (c Conversation) parse() domain.ConversationData {
	return domain.ConversationData{
		UUID:        c.UUID,
		Name:        c.Name,
		Description: c.Description,
		Picture:     c.Picture,
	}
}

func (f *frontend) getConversationsForInit() ([]InitConversation, error) {
	conversations, err := f.d.Conversation().GetConversations(domain.GetConversationOpts{
		Status: "active",
	})
	if err != nil {
		return nil, err
	}
	var resp []InitConversation
	for i := range conversations {
		resp = append(resp, InitConversation{
			Conversation: parseConversation(conversations[i].Data()),
			Messages:     []Message{parseMessage(conversations[i].GetLastMessage())},
		})
	}

	return resp, nil
}

func (f *frontend) onNewConversation(payload []byte) (interface{}, error) {
	var conversation NewConversation
	if err := json.Unmarshal(payload, &conversation); err != nil {
		return nil, err
	}

	if _, err := f.d.Conversation().NewConversation(domain.NewConversationValues{
		ConversationData: conversation.Conversation.parse(),
		Contacts:         conversation.Contacts,
		Server:           conversation.Servers,
	}); err != nil {
		return nil, err
	}

	return nil, nil
}

func (f *frontend) onGetConversation(payload []byte) (interface{}, error) {
	var uuid string

	if err := json.Unmarshal(payload, &uuid); err != nil {
		return nil, err
	}
	conv, err := f.d.Conversation().GetConversation(uuid)
	if err != nil {
		return nil, err
	}

	return parseConversation(conv.Data()), err
}

func (f *frontend) onGetConversations() (interface{}, error) {
	return f.d.Conversation().GetConversations(domain.GetConversationOpts{})
}

func (f *frontend) onGetConversationContacts(payload []byte) (interface{}, error) {
	var uuid string
	if err := json.Unmarshal(payload, &uuid); err != nil {
		return nil, err
	}

	_, err := f.d.Conversation().GetConversation(uuid)
	if err != nil {
		return nil, err
	}

	return nil, fmt.Errorf("Not Implementet. ")
}
