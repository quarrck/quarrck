package frontend

import (
	"encoding/json"

	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Contact struct {
	UUID    string
	Name    string
	Nick    string
	Status  string
	Picture string
}

type PubKey struct {
	Fingerprint string
	Type        string
	Data        string
}

type NewContact struct {
	Contact
	PublicKeys []PubKey
}

func (c *Contact) parse() domain.Contact {
	return domain.Contact{
		UUID:    c.UUID,
		Name:    c.Name,
		Nick:    c.Nick,
		Status:  c.Status,
		Picture: c.Picture,
	}
}

func (k PubKey) parse(contactUUID string) domain.PubKeyData {
	return domain.PubKeyData{
		Fingerprint: k.Fingerprint,
		Type:        k.Type,
		ContactUUID: contactUUID,
		Data:        k.Data,
	}
}

func parseContact(c domain.Contact) Contact {
	return Contact{
		UUID:    c.UUID,
		Name:    c.Name,
		Nick:    c.Nick,
		Status:  c.Status,
		Picture: c.Picture,
	}
}

func (f *frontend) onGetContact(payload []byte) (interface{}, error) {
	var uuid string

	if err := json.Unmarshal(payload, &uuid); err != nil {
		return nil, err
	}
	c, err := f.d.Contact().GetContact(uuid)
	if err != nil {
		return nil, err
	}

	return parseContact(*c), err
}

func (f *frontend) onNewContact(payload []byte) (interface{}, error) {
	var contact NewContact
	if err := json.Unmarshal(payload, &contact); err != nil {
		return nil, err
	}
	var keys []domain.PubKeyData
	for i := range contact.PublicKeys {
		keys = append(keys, contact.PublicKeys[i].parse(contact.UUID))
	}

	return nil, f.d.Contact().AddContact(contact.Contact.parse(), keys...)
}
