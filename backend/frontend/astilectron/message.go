package frontend

import (
	"encoding/json"
	"time"

	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Message struct {
	UUID     string
	From     string
	Topic    string
	Content  string
	Created  time.Time
	Received time.Time
}

func (msg *Message) parse() domain.Message {
	return domain.Message{
		UUID:       msg.UUID,
		AuthorUUID: msg.From,
		Topic:      msg.Topic,
		Content:    msg.Content,
		Created:    msg.Created,
		Received:   msg.Received,
	}
}

func parseMessage(msg domain.Message) Message {
	return Message{
		UUID:     msg.UUID,
		From:     msg.AuthorUUID,
		Topic:    msg.Topic,
		Content:  msg.Content,
		Created:  msg.Created,
		Received: msg.Received,
	}
}

type newMessage struct {
	Conversation string
	Message
}

func parseNewMessage(msg domain.Message) newMessage {
	return newMessage{
		Conversation: msg.ConversationUUID,
		Message:      parseMessage(msg),
	}
}

func (f *frontend) onNewMessage(payload []byte) (interface{}, error) {
	var msg newMessage

	if err := json.Unmarshal(payload, &msg); err != nil {
		f.d.Logger().Warnf("Error: unmarschal Message: %v", err)
		return nil, err
	}

	f.d.Logger().Debugf("New Message: %#v", msg)

	conv, err := f.d.Conversation().GetConversation(msg.Conversation)
	if err != nil {
		return nil, err
	}
	return conv.SendMessage(msg.Message.parse())
}

func (f *frontend) onGetMessages(payload []byte) (interface{}, error) {
	var opts domain.GetMessagesOpts

	if err := json.Unmarshal(payload, &opts); err != nil {
		f.d.Logger().Warnf("Error: unmarschal getMessagesRequest: %v", err)
		return nil, err
	}

	conv, err := f.d.Conversation().GetConversation(opts.ConversationUUID)
	if err != nil {
		return nil, err
	}

	if opts.Limit <= 0 || opts.Limit > messageLimit {
		opts.Limit = messageLimit
	}

	msgs, err := conv.GetMessages(opts)
	if err != nil {
		return nil, err
	}

	var resp []Message

	for i := range msgs {
		resp = append(resp, parseMessage(msgs[i]))
	}

	return resp, nil
}

func (f *frontend) onInviteConversationContacts(payload []byte) (interface{}, error) {
	var p struct {
		Conversation string
		Contacts     []string
	}

	if err := json.Unmarshal(payload, &p); err != nil {
		f.d.Logger().Warnf("Error: unmarschal getMessagesRequest: %v", err)
		return nil, err
	}

	conv, err := f.d.Conversation().GetConversation(p.Conversation)
	if err != nil {
		return nil, err
	}

	var contacts []*domain.Contact
	for i := range p.Contacts {
		contact, err := f.d.Contact().GetContact(p.Contacts[i])
		if err != nil {
			continue
		}

		contacts = append(contacts, contact)
	}

	return nil, conv.Invite(contacts...)
}
