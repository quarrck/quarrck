package frontend

import (
	"encoding/json"

	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Server struct {
	ID   uint
	Type string
	URL  string
}

func (s *Server) parse() domain.Server {
	return domain.Server{
		ID:   s.ID,
		Type: s.Type,
		URL:  s.URL,
	}
}

func parseServer(s domain.Server) Server {
	return Server{
		ID:   s.ID,
		Type: s.Type,
		URL:  s.URL,
	}
}

func (f *frontend) onGetServerTypes() (interface{}, error) {
	return f.d.Server().Types(), nil
}

func (f *frontend) onNewServer(payload []byte) (interface{}, error) {
	var s Server
	if err := json.Unmarshal(payload, &s); err != nil {
		return nil, err
	}
	return f.d.Server().AddServer(s.parse())
}

func (f *frontend) onGetServers() (interface{}, error) {
	s, err := f.d.Server().GetServers()
	if err != nil {
		return nil, err
	}
	var resp []Server
	for i := range s {
		resp = append(resp, parseServer(s[i]))
	}
	return resp, nil
}
