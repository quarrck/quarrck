package gorm

import (
	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Conversation struct {
	UUID        string `gorm:"primary_key"`
	Name        string
	Description string
	Picture     string
	Key         string
	Status      string
	Server      []ConversationServer  `gorm:"foreign_key:ConversationUUID"`
	Contacts    []ConversationContact `gorm:"foreign_key:ConversationUUID"`
}

type ConversationServer struct {
	ServerID         uint
	ConversationUUID string
	Order            uint
}

type ConversationContact struct {
	ContactUUID      string
	ConversationUUID string
}

func (c *conn) GetConversation(uuid string) (domain.ConversationData, error) {
	var conv Conversation
	if err := c.DB.First(&conv, Conversation{UUID: uuid}).Order("\"order\" ASC").Association("server").Find(&conv.Server).Error; err != nil { //
		return domain.ConversationData{}, err
	}

	return conv.parse(), nil
}

func (c *conn) GetConversations(opts domain.GetConversationOpts) ([]string, error) {

	s := c.DB.Select("uuid")

	if opts.Status != "" {
		s = s.Where("status = ?", opts.Status)
	}

	var convs []Conversation

	if err := s.Find(&convs).Error; err != nil {
		return nil, err
	}

	var resp []string

	for i := range convs {
		resp = append(resp, convs[i].UUID)
	}
	return resp, nil
}

func (c *conn) GetConversationContacts(uuid string) ([]string, error) {
	var contacts []ConversationContact
	if err := c.DB.Model(&Conversation{UUID: uuid}).Association("contacts").Find(&contacts).Error; err != nil {
		return nil, err
	}

	var resp []string

	for i := range contacts {
		resp = append(resp, contacts[i].ContactUUID)
	}

	return resp, nil
}

func (c *conn) GetConversationServers(uuid string) ([]uint, error) {
	var server []ConversationServer
	if err := c.DB.Model(&Conversation{UUID: uuid}).Order("\"order\" ASC").Association("server").Find(&server).Error; err != nil {
		return nil, err
	}

	var resp []uint

	for i := range server {
		resp = append(resp, server[i].ServerID)
	}

	return resp, nil
}

func (c *conn) AddConversationContact(conversationUUID, contactUUID string) error {
	return c.DB.Create(ConversationContact{
		ContactUUID:      contactUUID,
		ConversationUUID: conversationUUID,
	}).Error
}

func (c *conn) AddConversationServer(conversationUUID string, serverID uint) error {
	return c.DB.Create(ConversationServer{
		ServerID:         serverID,
		ConversationUUID: conversationUUID,
	}).Error
}

func (conn *conn) AddConversation(conv domain.ConversationData, contacts []string, server []uint) error {
	c := parseConversation(conv)

	for i := range server {
		c.Server = append(c.Server, ConversationServer{
			ServerID:         server[i],
			Order:            uint(i + 1),
			ConversationUUID: c.UUID,
		})
	}

	for i := range contacts {
		c.Contacts = append(c.Contacts, ConversationContact{
			ContactUUID:      contacts[i],
			ConversationUUID: c.UUID,
		})
	}

	return conn.DB.Create(&c).Error
}

func (c *conn) UpdateConversation(conv domain.ConversationData) error {
	return c.DB.Save(parseConversation(conv)).Error
}

func (conv *Conversation) parse() domain.ConversationData {
	return domain.ConversationData{
		UUID:        conv.UUID,
		Name:        conv.Name,
		Description: conv.Description,
		Key:         conv.Key,
		Picture:     conv.Picture,
		Status:      conv.Status,
	}
}

func parseConversation(c domain.ConversationData) Conversation {
	conv := Conversation{
		UUID:        c.UUID,
		Name:        c.Name,
		Description: c.Description,
		Key:         c.Key,
		Picture:     c.Picture,
		Status:      c.Status,
	}

	return conv
}
