package gorm

import (
	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Server struct {
	ID   uint `gorm:"primary_key"`
	Type string
	URL  string
}

func (c *conn) GetServers() ([]domain.Server, error) {
	var resp []domain.Server
	if err := c.Find(&resp).Error; err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *conn) GetServer(id uint) (domain.Server, error) {
	s := domain.Server{
		ID: id,
	}
	if err := c.First(&s).Error; err != nil {
		return s, err
	}

	return s, nil
}

func (c *conn) AddServer(s domain.Server) (uint, error) {
	srv := parseServer(s)
	r := c.Create(&srv)
	if r.Error != nil {
		return 0, r.Error
	}
	return srv.ID, nil
}

func (c *conn) UpdateServer(s domain.Server) error {
	return c.Save(s).Error
}
func (s *Server) parse() domain.Server {
	return domain.Server{
		ID:   s.ID,
		Type: s.Type,
		URL:  s.URL,
	}
}

func parseServer(s domain.Server) Server {
	return Server{
		ID:   s.ID,
		Type: s.Type,
		URL:  s.URL,
	}
}
