package gorm

import (
	"reflect"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"gitlab.com/quarrck/quarrck/backend/domain"
)

func Test_conn_GetServer(t *testing.T) {

	tests := []struct {
		name    string
		c       *conn
		want    []domain.Server
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.c.GetServer()
			if (err != nil) != tt.wantErr {
				t.Errorf("conn.GetServer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("conn.GetServer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_conn_SaveServer(t *testing.T) {
	type args struct {
		s domain.Server
	}
	tests := []struct {
		name    string
		c       *conn
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := tt.args.s
			if err := tt.c.AddServer(&s); (err != nil) != tt.wantErr {
				t.Errorf("conn.SaveServer() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
