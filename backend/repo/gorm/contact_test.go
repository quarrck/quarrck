package gorm

import (
	"reflect"
	"testing"

	"github.com/jinzhu/gorm"
)

func Test_conn_GetContacts(t *testing.T) {

	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		t.Error(err)
	}
	db = db.Debug()
	db.AutoMigrate(&Contact{})

	db.Create(&Contact{
		UUID: "Contact 1",
		Name: "Test 1",
	})

	db.Create(&Contact{
		UUID: "Contact 2",
		Name: "Test 2",
	})

	db.Create(&Contact{
		UUID: "Contact 3",
		Name: "Test 3",
	})

	tests := []struct {
		name    string
		want    []string
		wantErr bool
	}{
		{
			name: "Test 1",
			want: []string{
				"Contact 1", "Contact 2", "Contact 3",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &conn{
				db,
			}
			got, err := c.GetContacts()
			if (err != nil) != tt.wantErr {
				t.Errorf("conn.GetContacts() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("conn.GetContacts() = %v, want %v", got, tt.want)
			}
		})
	}
}
