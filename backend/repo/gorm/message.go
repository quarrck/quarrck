package gorm

import (
	"fmt"
	"time"

	"gitlab.com/quarrck/quarrck/backend/domain"
)

type Message struct {
	UUID             string `gorm:"primary_key"`
	From             string
	Topic            string
	Content          string
	CreatedAt        time.Time
	Recieved         *time.Time
	ConversationUUID string
}

func (c *conn) GetMessages(opts domain.GetMessagesOpts) ([]domain.Message, error) {
	if opts.ConversationUUID == "" {
		return nil, fmt.Errorf("Conversation UUID can't be empty. ")
	}
	query := c.Where("conversation_uuid = ?", opts.ConversationUUID)

	if !opts.From.IsZero() {
		query = query.Where("created_at > ?", opts.From)
	}

	if !opts.To.IsZero() {
		query = query.Where("created_at < ?", opts.To)
	}

	if opts.Offset > 0 {
		query = query.Offset(opts.Offset)
	}

	if opts.After != "" || opts.Before != "" {

		subQuery := query.Select("ROW_NUMBER() OVER ()  num, uuid").Table("messages").SubQuery()
		query = c.Joins("INNER JOIN ? AS row_num ON row_num.uuid = messages.uuid", subQuery)
		if err := query.Error; err != nil {
			return nil, err
		}

		if opts.After != "" {
			query = query.Where("num > ?", c.Raw("SELECT num FROM ? WHERE uuid = ?", subQuery, opts.After).SubQuery())
		}

		if err := query.Error; err != nil {
			return nil, err
		}

		if opts.Before != "" {
			query = query.Where("num < ?", c.Raw("SELECT num FROM ? WHERE uuid = ?", subQuery, opts.Before).SubQuery())
		}

		if err := query.Error; err != nil {
			return nil, err
		}
	}

	if opts.Limit <= 0 || opts.Limit > 50 {
		query = query.Limit(50)
	} else {
		query = query.Limit(opts.Limit)
	}

	var messages []Message

	if err := query.Find(&messages).Error; err != nil {
		return nil, err
	}

	var resp []domain.Message

	for i := range messages {
		resp = append(resp, messages[i].parse())
	}

	return resp, nil
}

func (c *conn) AddMessage(msg domain.Message) error {
	return c.Create(parseMessage(msg)).Error
}

func (c *conn) UpdateMessage(msg domain.Message) error {
	return c.Save(parseMessage(msg)).Error
}

func (msg *Message) parse() domain.Message {
	return domain.Message{
		UUID:       msg.UUID,
		AuthorUUID: msg.From,
		Topic:      msg.Topic,
		Created:    msg.CreatedAt,
		Content:    msg.Content,
	}
}

func parseMessage(msg domain.Message) Message {
	return Message{
		UUID:      msg.UUID,
		From:      msg.AuthorUUID,
		Topic:     msg.Topic,
		CreatedAt: msg.Created,
		Content:   msg.Content,
	}
}
