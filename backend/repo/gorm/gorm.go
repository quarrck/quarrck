package gorm

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/quarrck/quarrck/backend/domain"
)

type conn struct {
	*gorm.DB
}

type Repo interface {
	domain.ConfigRepo
	domain.ContactRepo
	domain.ServerRepo
	domain.MessageRepo
	domain.ConversationRepo
}

func Start(dialect string, args ...interface{}) (Repo, error) {
	db, err := gorm.Open(dialect, args...)
	if err != nil {
		return nil, err
	}

	if err := db.AutoMigrate(Contact{}, Conversation{}, ConversationServer{}, Server{}, Message{}, Config{}).Error; err != nil {
		return nil, err
	}
	c := conn{db.Debug()}

	return &c, nil
}
