package gorm

type Config struct {
	Key   string `gorm:"primary_key"`
	Value string
}

func (c *conn) GetConfig(key string) (string, error) {
	conf := &Config{
		Key: key,
	}
	if err := c.DB.First(conf).Error; err != nil {
		return "", err
	}

	return conf.Value, nil
}

func (c *conn) SaveConfig(key string, value string) error {
	return c.DB.Save(&Config{
		Key:   key,
		Value: value,
	}).Error
}
