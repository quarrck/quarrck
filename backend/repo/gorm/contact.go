package gorm

import "gitlab.com/quarrck/quarrck/backend/domain"

type Contact struct {
	UUID    string `gorm:"primary_key"`
	Name    string
	Nick    string
	Status  string
	Picture string
}

func (c *conn) GetContact(uuid string) (domain.Contact, error) {
	contact := Contact{
		UUID: uuid,
	}

	if err := c.First(&contact).Error; err != nil {
		return domain.Contact{}, err
	}

	return contact.parse(), nil
}

func (c *conn) GetContacts() ([]string, error) {
	var contacts []Contact
	if err := c.Select("uuid").Find(&contacts).Error; err != nil {
		return nil, err
	}

	var resp []string

	for i := range contacts {
		resp = append(resp, contacts[i].UUID)
	}

	return resp, nil
}

func (c *conn) AddContact(_ domain.Contact) error {
	panic("not implemented") // TODO: Implement
}

func (c *conn) UpdateContact(_ *domain.Contact) error {
	panic("not implemented") // TODO: Implement
}

func (c *Contact) parse() domain.Contact {
	return domain.Contact{
		UUID:    c.UUID,
		Name:    c.Name,
		Nick:    c.Nick,
		Status:  c.Status,
		Picture: c.Picture,
	}
}

func parseContact(c domain.Contact) Contact {
	return Contact{
		UUID:    c.UUID,
		Name:    c.Name,
		Nick:    c.Nick,
		Status:  c.Status,
		Picture: c.Picture,
	}
}
