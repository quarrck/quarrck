package gorm

import (
	"reflect"
	"testing"

	_ "github.com/mattn/go-sqlite3"

	"github.com/jinzhu/gorm"
	"gitlab.com/quarrck/quarrck/backend/domain"
)

func Test_conn_GetConversation(t *testing.T) {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		t.Error(err)
	}
	db = db.Debug()
	db.AutoMigrate(&Conversation{}, &ConversationServer{})

	db.Create(&Conversation{
		UUID: "Conv 1",
		Name: "Test 1",
		Server: []ConversationServer{
			{
				ServerID: 1,
				Order:    1,
			},
			{
				ServerID: 2,
				Order:    2,
			},
			{
				ServerID: 3,
				Order:    3,
			},
		},
	})

	db.Create(&Conversation{
		UUID: "Conv 2",
		Name: "Test 2",
		Server: []ConversationServer{
			{
				ServerID: 1,
				Order:    1,
			},
			{
				ServerID: 2,
				Order:    3,
			},
			{
				ServerID: 3,
				Order:    2,
			},
		},
	})

	db.Create(&Conversation{
		UUID: "Conv 3",
		Name: "Test 3",
		Server: []ConversationServer{
			{
				ServerID: 1,
				Order:    3,
			},
			{
				ServerID: 2,
				Order:    1,
			},
			{
				ServerID: 3,
				Order:    2,
			},
		},
	})

	type args struct {
		uuid string
	}
	tests := []struct {
		name    string
		uuid    string
		want    domain.Conversation
		wantErr bool
	}{
		{
			name: "Test 2",
			uuid: "Conv 2",
			want: domain.Conversation{
				UUID: "Conv 2",
				Name: "Test 2",
				Server: []uint{
					1,
					3,
					2,
				},
			},
		},
	}
	for _, tt := range tests {

		c := conn{DB: db}
		t.Run(tt.name, func(t *testing.T) {

			got, err := c.GetConversation(tt.uuid)
			if (err != nil) != tt.wantErr {
				t.Errorf("conn.GetConversation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("conn.GetConversation():\n\tgot:  %#v,\n\twant: %#v", got, tt.want)
			}
		})
	}
}
