package gorm

import (
	"testing"
	"time"

	_ "github.com/mattn/go-sqlite3"

	"github.com/jinzhu/gorm"
	"gitlab.com/quarrck/quarrck/backend/domain"
)

func Test_conn_GetMessages(t *testing.T) {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		t.Error(err)
	}
	db = db.Debug()
	db.AutoMigrate(&Message{})

	if err := db.Create(&Message{
		UUID:             "Test1",
		CreatedAt:        time.Date(2020, time.November, 3, 7, 0, 0, 0, time.Local),
		ConversationUUID: "TestConv",
	}).Error; err != nil {
		t.Error(err)
	}

	if err := db.Create(&Message{
		UUID:             "Test2",
		CreatedAt:        time.Date(2020, time.November, 3, 8, 0, 0, 0, time.Local),
		ConversationUUID: "TestConv",
	}).Error; err != nil {
		t.Error(err)
	}

	if err := db.Create(&Message{
		UUID:             "Test3",
		CreatedAt:        time.Date(2020, time.November, 3, 9, 0, 0, 0, time.Local),
		ConversationUUID: "TestConv",
	}).Error; err != nil {
		t.Error(err)
	}

	if err := db.Create(&Message{
		UUID:             "Test4",
		CreatedAt:        time.Date(2020, time.November, 3, 10, 0, 0, 0, time.Local),
		ConversationUUID: "TestConv",
	}).Error; err != nil {
		t.Error(err)
	}

	if err := db.Create(&Message{
		UUID:             "Test5",
		CreatedAt:        time.Date(2020, time.November, 3, 11, 0, 0, 0, time.Local),
		ConversationUUID: "TestConv",
	}).Error; err != nil {
		t.Error(err)
	}

	tests := []struct {
		name    string
		opts    domain.GetMessagesOpts
		want    []domain.Message
		wantErr bool
	}{
		{
			name: "Get by Time",
			opts: domain.GetMessagesOpts{
				Conversation: "TestConv",
				Limit:        3,
				From:         time.Date(2020, time.November, 3, 7, 30, 0, 0, time.Local),
				To:           time.Date(2020, time.November, 3, 10, 30, 0, 0, time.Local),
			},
			want: []domain.Message{
				{
					UUID:    "Test2",
					Created: time.Date(2020, time.November, 3, 8, 0, 0, 0, time.Local),
				},
				{
					UUID:    "Test3",
					Created: time.Date(2020, time.November, 3, 9, 0, 0, 0, time.Local),
				},
				{
					UUID:    "Test4",
					Created: time.Date(2020, time.November, 3, 10, 0, 0, 0, time.Local),
				},
			},
		},
		{
			name: "After and before",
			opts: domain.GetMessagesOpts{
				Conversation: "TestConv",
				Limit:        3,
				After:        "Test1",
				Before:       "Test5",
			},
			want: []domain.Message{
				{
					UUID:    "Test2",
					Created: time.Date(2020, time.November, 3, 8, 0, 0, 0, time.Local),
				},
				{
					UUID:    "Test3",
					Created: time.Date(2020, time.November, 3, 9, 0, 0, 0, time.Local),
				},
				{
					UUID:    "Test4",
					Created: time.Date(2020, time.November, 3, 10, 0, 0, 0, time.Local),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &conn{
				DB: db,
			}
			got, err := c.GetMessages(tt.opts)
			if (err != nil) != tt.wantErr {
				t.Errorf("conn.GetMessages() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != len(tt.want) {
				t.Errorf("len(got) != len(tt.want)")
			}
			for i := range got {
				if got[i].UUID != tt.want[i].UUID {
					t.Errorf("UUID unequal got: %s, want: %s", got[i].UUID, tt.want[i].UUID)
				}

				if !got[i].Created.Equal(tt.want[i].Created) {
					t.Errorf("Created unequal got: %v, want: %v", got[i].Created, tt.want[i].Created)
				}
			}
		})
	}
	t.Fail()
}
