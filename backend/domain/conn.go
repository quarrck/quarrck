package domain

type Connector func(Server, User, ConversationHandler, Logger) (Conn, error)

type Server struct {
	ID   uint
	Type string
	URL  string //url.URL
}

type Conn interface {
	Send(Package, ...*Contact) error
	Status() string
	Close()
	Closed() bool
}
