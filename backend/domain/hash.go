package domain

import (
	"crypto"
	"hash"
)

type Hash struct {
	ID crypto.Hash
	hash.Hash
}

func GetHash(id crypto.Hash) Hash {
	return Hash{ID: id, Hash: id.New()}
}
