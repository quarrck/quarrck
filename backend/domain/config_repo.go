package domain

type ConfigRepo interface {
	GetConfig(string) (string, error)
	SaveConfig(string, string) error
}

func (d *Domain) RegisterConfigRepo(c ConfigRepo) error {
	if d.configRepo != nil {
		d.configRepo = c
	}
	return nil
}
