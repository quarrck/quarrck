package domain

type Package struct {
	Version          string
	From             string
	Fingerprint      string
	ConversationUUID string
	Type             PackageType
	Content          []byte
	Signatur         []byte
}

type PackageType string

var (
	PackageTypeMSG PackageType = "MSG"
)

type Sender interface {
	Send(Package) error
	SendMessage(*Conversation, Message)
}

type Receiver interface {
	Receive(Package) error
}
