package domain

import "sync"

var (
	frontendOnce sync.Once
	frontend     Frontend
)

type Frontend interface {
	NewMessage(*Message) error
}

func RegisterFrontend(f Frontend) {
	frontendOnce.Do(func() {
		frontend = f
	})
}
