package domain

import (
	"fmt"
	"sync"
)

type PubKeyHandler interface {
	GetPubKey(string) (PubKey, error)
	GetPubKeysByContact(string) (map[string]PubKey, error)
	AddPubKey(PubKeyData) error
	RegisterPubKeyParser(string, PubKeyParser)
}

type PubKeyRepo interface {
	GetPubKey(string) (PubKeyData, error)
	GetPubKeysByContact(string) ([]string, error)
	AddPubKey(PubKeyData) error
}

type pubKeyHandler struct {
	repo   PubKeyRepo
	lock   sync.Mutex
	cache  map[string]pubKeyCacheEntry
	parser map[string]PubKeyParser
	bus    Bus
	logger Logger
}

type pubKeyCacheEntry struct {
	t string
	p PubKey
}

func (d *Domain) RegisterPubKeyRepo(r PubKeyRepo) error {
	if d.pubKeyHandler == nil {
		d.pubKeyHandler = &pubKeyHandler{
			repo:   r,
			cache:  make(map[string]pubKeyCacheEntry),
			parser: make(map[string]PubKeyParser),
			bus:    d.bus,
			logger: d.logger,
		}
	}

	return nil
}

func (d *Domain) RegisterPubKeyParser(t string, p PubKeyParser) error {
	if d.pubKeyHandler == nil {
		return fmt.Errorf("Register a PubKeyRepo first. ")
	}

	d.pubKeyHandler.RegisterPubKeyParser(t, p)
	return nil
}

func (h *pubKeyHandler) RegisterPubKeyParser(t string, p PubKeyParser) {
	h.lock.Lock()
	if _, ok := h.parser[t]; ok {
		for k, v := range h.cache {
			if v.t != t {
				continue
			}
			d, err := h.repo.GetPubKey(k)
			if err != nil {
				h.logger.WithField("PubKeyFingerprint", k).WithError(err).Warnf("Error get pub key from repo. ")
				delete(h.cache, k)
				continue
			}
			v.p, err = p(d)
			if err != nil {
				h.logger.WithField("PubKeyFingerprint", k).WithError(err).Warnf("Error parse pub key. ")
				delete(h.cache, k)
				continue
			}
			h.cache[k] = v
		}
	}

	h.parser[t] = p

	h.lock.Unlock()

	h.bus.Publish("pubkey_parser_registert", t)
}

func (h *pubKeyHandler) GetPubKey(fingerprint string) (PubKey, error) {
	h.lock.Lock()
	defer h.lock.Unlock()
	if e, ok := h.cache[fingerprint]; ok {
		return e.p, nil
	}

	if h.repo == nil {
		return nil, notRegistertErr{}
	}

	d, err := h.repo.GetPubKey(fingerprint)
	if err != nil {
		return nil, err
	}

	parse, ok := h.parser[d.Type]
	if !ok {
		return nil, fmt.Errorf("No pub key parser found for %s. ", d.Type)
	}

	p, err := parse(d)
	if err != nil {
		return nil, err
	}

	h.cache[fingerprint] = pubKeyCacheEntry{
		t: d.Type,
		p: p,
	}

	return p, nil
}

func (h *pubKeyHandler) GetPubKeysByContact(contactUUID string) (map[string]PubKey, error) {
	fingerprints, err := h.repo.GetPubKeysByContact(contactUUID)
	if err != nil {
		return nil, err
	}

	keys := map[string]PubKey{}
	for i := range fingerprints {
		key, err := h.GetPubKey(fingerprints[i])
		if err != nil {
			h.logger.WithField("PubKeyFingerprint", fingerprints[i]).WithError(err).Warnf("Error get pub key. ")
			continue
		}

		keys[fingerprints[i]] = key
	}
	return keys, nil
}

func (h *pubKeyHandler) AddPubKey(d PubKeyData) error {
	return h.addPubKey(d)
}

func (h *pubKeyHandler) addPubKey(p PubKeyData) error {
	if p.Fingerprint == "" {
		return fmt.Errorf("Empty fingerprint. ")
	}
	if p.Type == "" {
		return fmt.Errorf("Empty type. ")
	}

	err := h.repo.AddPubKey(p)
	if err != nil {
		return err
	}

	h.bus.Publish("PubKeyAdded", p.Fingerprint, p.ContactUUID)

	return nil
}
