package domain

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"sync"

	"github.com/vmihailenco/msgpack"

	"github.com/google/uuid"
)

type ConversationData struct {
	UUID        string
	Name        string
	Description string
	Picture     string
	Key         string
	Status      string
}

type GetConversationOpts struct {
	Status string
}

type conversation struct {
	user            User
	data            ConversationData
	conns           map[uint]Conn
	contacts        map[string]*Contact
	lock            sync.Mutex
	lastMessage     Message
	repo            ConversationRepo
	messages        MessageHandler
	contactsHandler ContactHandler
	connHandler     ConnHandler
	cipher          cipher.Block
	bus             Bus
	logger          Logger
}

type Conversation interface {
	UUID() string
	Status() string
	Data() ConversationData
	GetContact(string) (*Contact, error)
	Encrypt([]byte) ([]byte, error)
	Decrypt([]byte) ([]byte, error)
	SendMessage(Message) (string, error)
	ReceiveMessage(Message) error
	GetLastMessage() Message
	GetMessages(GetMessagesOpts) ([]Message, error)
	Invite(...*Contact) error
}

func newConversation(data ConversationData, user User, messages MessageHandler, repo ConversationRepo, contactsHandler ContactHandler, connHandler ConnHandler, bus Bus, logger Logger) (Conversation, error) {

	c := conversation{
		user:            user,
		data:            data,
		messages:        messages,
		repo:            repo,
		contactsHandler: contactsHandler,
		connHandler:     connHandler,
		contacts:        map[string]*Contact{},
		conns:           map[uint]Conn{},
		bus:             bus,
		logger:          logger.WithField("ConversationUUID", data.UUID),
	}

	if contactUUIDs, err := c.repo.GetConversationContacts(c.data.UUID); err == nil {
		for i := range contactUUIDs {
			contact, err := c.contactsHandler.GetContact(contactUUIDs[i])
			if err != nil {
				c.logger.Warnf("Error get contact %s for conversation %s: %v", contactUUIDs[i], c.data.UUID, err)
				continue
			}
			c.contacts[contactUUIDs[i]] = contact
		}
	} else {
		return nil, err
	}

	if c.data.Status == "active" {
		key, err := hex.DecodeString(c.data.Key)
		if err != nil {
			return nil, fmt.Errorf("Error decode key: %v", err)
		}

		cipher, err := aes.NewCipher(key)
		if err != nil {
			return nil, fmt.Errorf("Error make cipher: %v", err)
		}
		c.cipher = cipher

		servers, err := c.repo.GetConversationServers(c.data.UUID)
		if err != nil {
			return nil, err
		}
		for i := range servers {
			conn, err := c.connHandler.GetConn(servers[i])
			if err != nil {
				c.logger.WithError(err).Warn("Error Get conn. ")
			}
			c.conns[servers[i]] = conn
		}
	}

	return &c, nil
}

func (c *conversation) UUID() string {
	return c.data.UUID
}

func (c *conversation) Status() string {
	return c.data.Status
}

func (c *conversation) Data() ConversationData {
	return c.data
}

func (c *conversation) GetContact(uuid string) (*Contact, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if contact, ok := c.contacts[uuid]; ok {
		return contact, nil
	}
	return nil, fmt.Errorf("Contact not found")
}

func (c *conversation) AddContact(contact *Contact) error {
	if err := c.repo.AddConversationContact(c.UUID(), contact.UUID); err != nil {
		return err
	}

	c.lock.Lock()
	c.contacts[contact.UUID] = contact
	c.lock.Unlock()

	return nil
}

func (c *conversation) GetLastMessage() Message {
	c.lock.Lock()
	defer c.lock.Unlock()
	return c.lastMessage
}

func (c *conversation) GetMessages(opts GetMessagesOpts) ([]Message, error) {
	opts.ConversationUUID = c.data.UUID
	return c.messages.GetMessages(opts)
}

func (c *conversation) to() []*Contact {
	if c.contacts == nil {
		return nil
	}

	var contacts []*Contact
	for i := range c.contacts {
		contacts = append(contacts, c.contacts[i])
	}

	return contacts
}

func (c *conversation) SendMessage(msg Message) (string, error) {
	if c.data.Status != "active" {
		return "", fmt.Errorf("Could not send message on inactive convesation")
	}
	if msg.UUID == "" {
		msg.UUID = uuid.New().String()
	}

	msg.AuthorUUID = c.user.UUID()
	msg.ConversationUUID = c.data.UUID

	if err := c.addMessage(msg); err != nil {
		return msg.UUID, err
	}

	content, err := msgpack.Marshal(msg)
	if err != nil {
		return msg.UUID, err
	}

	content, err = c.Encrypt(content)
	if err != nil {
		return msg.UUID, err
	}

	hash := sha512.New()
	hash.Write(content)

	signatur, err := c.user.Sign(hash.Sum(nil))

	to := c.to()
	pkg := Package{
		Version:          "0.1",
		From:             c.user.UUID(),
		Fingerprint:      c.user.Fingerprint(),
		ConversationUUID: c.data.UUID,
		Type:             PackageTypeMSG,
		Content:          content,
		Signatur:         signatur,
	}

	for i := range c.conns {
		if err := c.conns[i].Send(pkg, to...); err != nil {
			c.logger.WithError(err).WithField("MessageUUID", msg.UUID).Warn("Error while sending message. ")
		}
	}
	return msg.UUID, nil
}

func (c *conversation) ReceiveMessage(msg Message) error {
	return c.addMessage(msg)
}

func (c *conversation) Encrypt(data []byte) ([]byte, error) {
	iv := make([]byte, c.cipher.BlockSize())

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	buf := bytes.NewBuffer(nil)

	if _, err := buf.Write(iv); err != nil {
		return nil, err
	}

	if _, err := (&cipher.StreamWriter{S: cipher.NewOFB(c.cipher, iv), W: buf}).Write(data); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func (c *conversation) Decrypt(data []byte) ([]byte, error) {
	if c.cipher == nil {
		return nil, fmt.Errorf("No cipher")
	}
	bs := c.cipher.BlockSize()
	iv := make([]byte, bs)
	iv, data = data[:bs], data[bs:]

	return ioutil.ReadAll(&cipher.StreamReader{S: cipher.NewOFB(c.cipher, iv), R: bytes.NewBuffer(data)})
}

func (c *conversation) AddMessage(msg Message) error {
	if msg.UUID == "" {
		return fmt.Errorf("UUID should not be empty. ")
	}
	msg.ConversationUUID = c.data.UUID
	return c.addMessage(msg)
}

func (c *conversation) addMessage(msg Message) error {
	if err := c.messages.AddMessage(msg); err != nil {
		return err
	}

	c.lock.Lock()
	if msg.Created.After(c.lastMessage.Created) {
		c.lastMessage = msg
	}
	c.lock.Unlock()

	if c.bus != nil {
		c.bus.Publish("new_message", Conversation(c), msg)
	}
	return nil
}

func (c *conversation) AcceptInvite() error {
	switch c.data.Status {
	case "active":
		return nil
	case "invited":
		c.data.Status = "active"
		return c.repo.UpdateConversation(c.data)
	default:
		return fmt.Errorf("Could not Accept Invite for conversation %s with status %s. ", c.data.UUID, c.data.Status)
	}
}

func (c *conversation) RejectInvite() error {
	switch c.data.Status {
	case "rejected":
		return nil
	case "invited":
		c.data.Status = "rejected"
		return c.repo.UpdateConversation(c.data)
	default:
		return fmt.Errorf("Could not Reject Invite for conversation %s with status %s. ", c.data.UUID, c.data.Status)
	}
}

func (c *conversation) Invite(contacts ...*Contact) error {
	return nil
}
