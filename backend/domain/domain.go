package domain

import (
	"github.com/asaskevich/EventBus"
)

type Domain struct {
	user                User
	configRepo          ConfigRepo
	pubKeyHandler       PubKeyHandler
	conversationHandler ConversationHandler
	messageHandler      MessageHandler
	contactHandler      ContactHandler
	serverHandler       ServerHandler
	bus                 Bus
	logger              Logger
}

type User interface {
	UUID() string
	Fingerprint() string
	Sign([]byte) ([]byte, error)
	Decrypt([]byte) ([]byte, error)
}

type Bus interface {
	EventBus.Bus
}

func New(user User, logger Logger) (*Domain, error) {
	d := Domain{
		user:   user,
		bus:    EventBus.New(),
		logger: logger,
	}

	return &d, nil
}

func (d *Domain) StartServer() error {
	if d.serverHandler != nil {
		return d.serverHandler.StartServer(d.conversationHandler)
	}
	return nil
}

func (d *Domain) User() User {
	return d.user
}

func (d *Domain) Bus() Bus {
	return d.bus
}
