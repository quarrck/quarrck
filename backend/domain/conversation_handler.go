package domain

import (
	"crypto/sha512"
	"fmt"

	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack"
)

type ConversationHandler interface {
	GetConversation(string) (Conversation, error)
	GetConversations(GetConversationOpts) ([]Conversation, error)
	NewConversation(NewConversationValues) (Conversation, error)
	Receive(Package) error
}

type conversationHandler struct {
	user     User
	repo     ConversationRepo
	contacts ContactHandler
	messages MessageHandler
	conns    ConnHandler
	cache    map[string]Conversation
	bus      Bus
	logger   Logger
}

func newConversationHandler(repo ConversationRepo, user User, contacts ContactHandler, messages MessageHandler, conns ServerHandler, bus Bus, logger Logger) ConversationHandler {
	return &conversationHandler{
		user:     user,
		cache:    map[string]Conversation{},
		contacts: contacts,
		messages: messages,
		conns:    conns,
		repo:     repo,
		bus:      bus,
		logger:   logger,
	}
}

func (d *Domain) RegisterConversationRepo(r ConversationRepo) error {
	if d.contactHandler == nil {
		return fmt.Errorf("Register contact repo first. ")
	}
	if d.messageHandler == nil {
		return fmt.Errorf("Register message repo first. ")
	}
	if d.conversationHandler == nil {
		d.conversationHandler = newConversationHandler(r, d.user, d.contactHandler, d.messageHandler, d.serverHandler, d.bus, d.logger)
	}
	return nil
}

func (d *Domain) Conversation() ConversationHandler {
	return d.conversationHandler
}

type ConversationRepo interface {
	GetConversation(string) (ConversationData, error)
	GetConversationContacts(string) ([]string, error)
	GetConversationServers(string) ([]uint, error)
	GetConversations(GetConversationOpts) ([]string, error)
	AddConversation(ConversationData, []string, []uint) error
	AddConversationContact(string, string) error
	AddConversationServer(string, uint) error
	UpdateConversation(ConversationData) error
}

type NewConversationValues struct {
	ConversationData
	Server   []uint
	Contacts []string
}

type ConversationInviteValues struct {
	UUID        string
	Name        string
	Description string
	Picture     string
	Server      []uint
	Contacts    []string
}

func (h *conversationHandler) GetConversation(uuid string) (Conversation, error) {
	return h.getConversation(uuid)
}

func (h *conversationHandler) getConversation(uuid string) (Conversation, error) {
	if uuid == "" {
		return nil, fmt.Errorf("UUID is empty. ")
	}
	if conv, ok := h.cache[uuid]; !ok && h.repo == nil {
		return nil, notRegistertErr{}
	} else if ok {
		return conv, nil
	}
	data, err := h.repo.GetConversation(uuid)
	if err != nil {
		return nil, err
	}
	return h.addConversation(data)
}

func (h *conversationHandler) addConversation(data ConversationData) (Conversation, error) {
	conv, err := newConversation(data, h.user, h.messages, h.repo, h.contacts, h.conns, h.bus, h.logger)
	if err != nil {
		return nil, err
	}

	h.cache[data.UUID] = conv

	return conv, nil
}

func (h *conversationHandler) GetConversations(opts GetConversationOpts) ([]Conversation, error) {
	uuids, err := h.repo.GetConversations(opts)
	if err != nil {
		return nil, err
	}

	var conversations []Conversation

	for i := range uuids {
		c, err := h.getConversation(uuids[i])
		if err != nil {
			h.logger.WithField("ConversationUUID", uuids[i]).WithError(err).Warnf("Error get conversation. ")
			continue
		}
		conversations = append(conversations, c)
	}

	return conversations, nil
}

func (h *conversationHandler) NewConversation(values NewConversationValues) (Conversation, error) {
	data := values.ConversationData
	data.UUID = uuid.New().String()
	data.Status = "created"
	return h.newConversation(data, values.Contacts, values.Server)
}

func (h *conversationHandler) NewConversationInvite(values ConversationInviteValues) error {
	if values.UUID == "" {
		return fmt.Errorf("UUID is empty. ")
	}

	if conv, err := h.getConversation(values.UUID); err == nil {
		switch conv.Status() {
		case "refused":
			return fmt.Errorf("Conversation already refused")
		default:
			return fmt.Errorf("Allready have Conversation")
		}
	}

	_, err := h.addConversation(ConversationData{
		UUID:        values.UUID,
		Name:        values.Name,
		Description: values.Description,
		Picture:     values.Picture,
		Status:      "invited",
	})

	return err
}

func (h *conversationHandler) newConversation(data ConversationData, contacts []string, server []uint) (Conversation, error) {

	if err := h.repo.AddConversation(data, contacts, server); err != nil {
		return nil, err
	}

	return h.addConversation(data)
}

func (h *conversationHandler) Receive(pkg Package) error {
	hash := sha512.New()
	hash.Write(pkg.Content)

	switch pkg.Type {
	case PackageTypeMSG:
		conv, err := h.getConversation(pkg.ConversationUUID)
		if err != nil {
			return err
		}

		if conv.Status() != "active" {
			return fmt.Errorf("Conversation is not active")
		}

		c, err := conv.GetContact(pkg.From)
		if err != nil {
			return err
		}

		if err := c.Validate(pkg.Fingerprint, pkg.Signatur, hash.Sum(nil)); err != nil {
			return err
		}

		content, err := conv.Decrypt(pkg.Content)
		if err != nil {
			return err
		}

		var msg Message

		if err := msgpack.Unmarshal(content, &msg); err != nil {
			return err
		}

		return conv.ReceiveMessage(msg)

	default:
		return fmt.Errorf("Unkown Package Type %s. ", pkg.Type)

	}

}
