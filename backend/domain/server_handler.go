package domain

import (
	"fmt"
	"sync"
)

var (
	serverOnce sync.Once
)

type ServerHandler interface {
	ConnHandler
	Types() []string
	AddServer(Server) (uint, error)
	GetServers() ([]Server, error)
	StartServer(ConversationHandler) error
	RegisterConnector(string, Connector)
}

type ConnHandler interface {
	GetConn(id uint) (Conn, error)
}
type ServerRepo interface {
	GetServer(uint) (Server, error)
	GetServers() ([]Server, error)
	AddServer(Server) (uint, error)
	UpdateServer(Server) error
}

type serverHandler struct {
	user          User
	repo          ServerRepo
	conversations ConversationHandler
	connectors    map[string]Connector
	connections   map[uint]Conn
	lock          sync.Mutex
	bus           Bus
	logger        Logger
}

func newServerHandler(repo ServerRepo, user User, bus Bus, logger Logger) (ServerHandler, error) {
	h := serverHandler{
		repo:       repo,
		user:       user,
		connectors: map[string]Connector{},
		bus:        bus,
		logger:     logger,
	}

	return &h, nil
}

func (d *Domain) RegisterServerRepo(r ServerRepo) error {
	if d.serverHandler == nil {
		var err error
		d.serverHandler, err = newServerHandler(r, d.user, d.bus, d.logger)
		return err
	}
	return nil
}

func (d *Domain) RegisterConnector(t string, c Connector) error {
	if d.serverHandler == nil {
		return fmt.Errorf("Register server repo first. ")
	}
	d.serverHandler.RegisterConnector(t, c)
	return nil
}

func (d *Domain) Server() ServerHandler {
	return d.serverHandler
}

func (h *serverHandler) StartServer(conversations ConversationHandler) error {
	h.connections = map[uint]Conn{}
	h.conversations = conversations

	s, err := h.repo.GetServers()
	if err != nil {
		return fmt.Errorf("Error get server: %v", err)
	}

	for i := range s {
		c, err := h.startServer(s[i])
		if err != nil {
			h.logger.Warn(err)
		}
		h.connections[s[i].ID] = c
	}

	return nil
}

func (h *serverHandler) AddServer(s Server) (uint, error) {
	return h.repo.AddServer(s)
}

func (h *serverHandler) GetServers() ([]Server, error) {
	return h.repo.GetServers()
}

func (h *serverHandler) GetConn(id uint) (Conn, error) {
	if h.connections == nil {
		return nil, fmt.Errorf("No Server Start. ")
	}

	if c, ok := h.connections[id]; ok {
		return c, nil
	}

	s, err := h.repo.GetServer(id)
	if err != nil {
		return nil, err
	}
	return h.startServer(s)
}

func (h *serverHandler) RegisterConnector(t string, c Connector) {
	h.connectors[t] = c
}

func (h *serverHandler) Types() []string {
	if h.connectors == nil {
		return nil
	}
	var types []string
	for t := range h.connectors {
		types = append(types, t)
	}
	return types
}

func (h *serverHandler) startServer(s Server) (Conn, error) {
	if h.connectors[s.Type] == nil {
		return nil, fmt.Errorf("No Connector found for %s", s.Type)
	}

	c, err := h.connectors[s.Type](s, h.user, h.conversations, h.logger)
	if err != nil {
		return nil, err
	}
	h.connections[s.ID] = c

	return c, nil
}
