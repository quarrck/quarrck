package domain

import (
	"fmt"
)

type Contact struct {
	UUID    string
	Name    string
	Nick    string
	Status  string
	Picture string
	pubKeys map[string]PubKey
}

func (c *Contact) Validate(fingerprint string, signatur, hash []byte) error {
	if p, ok := c.pubKeys[fingerprint]; ok {
		return p.Validate(signatur, hash)
	}
	return fmt.Errorf("No PubKey found for %s. ")
}
