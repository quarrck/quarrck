package domain

import "time"

type Message struct {
	UUID             string
	AuthorUUID       string
	Topic            string
	Content          string
	Created          time.Time
	Received         time.Time
	ConversationUUID string
}

type GetMessagesOpts struct {
	ConversationUUID string
	AuthorUUID       string
	Limit            int
	Offset           int
	After            string
	Before           string
	Around           string
	From             time.Time
	To               time.Time
}
