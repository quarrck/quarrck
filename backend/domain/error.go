package domain

type DomainError interface {
	error
	Fields()
}

type notRegistertErr struct{}

func (notRegistertErr) Error() string {
	return "Not Registert"
}
