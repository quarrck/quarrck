package domain

import "fmt"

type MessageRepo interface {
	GetMessages(GetMessagesOpts) ([]Message, error)
	AddMessage(Message) error
}

type MessageHandler interface {
	AddMessage(Message) error
	GetMessages(GetMessagesOpts) ([]Message, error)
}

type messageHandler struct {
	domain *Domain
	repo   MessageRepo
	bus    Bus
	logger Logger
}

func (d *Domain) RegisterMessageRepo(r MessageRepo) error {
	if d.messageHandler == nil {
		d.messageHandler = newMessageHandler(r, d.bus, d.logger)
	}
	return nil
}

func newMessageHandler(r MessageRepo, bus Bus, logger Logger) MessageHandler {
	h := &messageHandler{
		repo:   r,
		bus:    bus,
		logger: logger,
	}
	return h
}

func (h *messageHandler) GetMessages(opts GetMessagesOpts) ([]Message, error) {
	if opts.ConversationUUID == "" {
		return nil, fmt.Errorf("Empty conversation UUID")
	}

	if opts.Limit <= 0 || opts.Limit > 50 {
		opts.Limit = 50
	}
	return h.repo.GetMessages(opts)
}

func (h *messageHandler) AddMessage(m Message) error {
	if m.ConversationUUID == "" {
		return fmt.Errorf("Empty conversation UUID")
	}
	if m.UUID == "" {
		return fmt.Errorf("Empty message UUID")
	}
	if err := h.repo.AddMessage(m); err != nil {
		return err
	}
	h.bus.Publish("MessageAdded", m.UUID)
	return nil
}
