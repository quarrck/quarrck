package domain

var (
	id string
)

type Repository interface {
	ConfigRepo
	ContactRepo
	ConversationRepo
	MessageRepo
	ServerRepo
}

func (d *Domain) RegisterRepository(r Repository) {
	d.RegisterConfigRepo(r)
	d.RegisterContactRepo(r)
	d.RegisterConversationRepo(r)
	d.RegisterMessageRepo(r)
	d.RegisterServerRepo(r)
}

/*
func SetUUID(uuid string) {
	id = uuid
	if err := configRepo.SaveConfig("UUID", id); err != nil {
		logger.Warnf("Error set UUID: %v", err)
	}
}

func GetUUID() string {
	if id == "" {
		var err error
		id, err = configRepo.GetConfig("UUID")
		if err != nil {
			logger.Warnf("Error get UUID: %v", err)
			return id
		}
	}
	return id
}
*/
