package domain

import (
	"fmt"
	"sync"
)

type ContactHandler interface {
	AddContact(Contact, ...PubKeyData) error
	GetContact(string) (*Contact, error)
	GetContacts() ([]*Contact, error)
}

type contactHandler struct {
	pubKeys PubKeyHandler
	repo    ContactRepo
	cache   map[string]*Contact
	lock    sync.Mutex
	bus     Bus
	logger  Logger
}

func (d *Domain) RegisterContactRepo(repo ContactRepo) error {
	if d.pubKeyHandler == nil {
		return fmt.Errorf("Register Pub Key Repo first. ")
	}
	if d.contactHandler != nil {
		return fmt.Errorf("Contact Repo already registert. ")
	}
	d.contactHandler = newContactHandler(repo, d.pubKeyHandler, d.bus, d.logger)

	return nil
}

func (d *Domain) Contact() ContactHandler {
	return d.contactHandler
}

type ContactRepo interface {
	GetContact(string) (Contact, error)
	GetContacts() ([]string, error)
	AddContact(Contact) error
	UpdateContact(*Contact) error
}

func newContactHandler(repo ContactRepo, pubKeys PubKeyHandler, bus Bus, logger Logger) ContactHandler {
	h := contactHandler{
		pubKeys: pubKeys,
		repo:    repo,
		cache:   map[string]*Contact{},
		bus:     bus,
		logger:  logger,
	}

	h.bus.Subscribe("PubKeyAdded", h.onPubKeyAdded)

	return &h
}

func (h *contactHandler) onPubKeyAdded(fingerprint, contactUUId string) {
	h.lock.Lock()
	defer h.lock.Unlock()
	if c, ok := h.cache[contactUUId]; ok {
		if p, err := h.pubKeys.GetPubKey(fingerprint); err != nil {
			h.logger.WithField("ContactUUID", c.UUID).WithField("PubKeyFingerprint", fingerprint).WithError(err).Warnf("Error get pub key. ")
		} else {
			c.pubKeys[fingerprint] = p
		}
	}
}

func (h *contactHandler) GetContact(id string) (*Contact, error) {
	if c, ok := h.cache[id]; ok {
		return c, nil
	}

	c, err := h.repo.GetContact(id)
	if err != nil {
		return nil, err
	}

	c.pubKeys, err = h.pubKeys.GetPubKeysByContact(id)
	if err != nil {
		return nil, err
	}

	h.cache[id] = &c

	return &c, nil

}

func (h *contactHandler) GetContacts() ([]*Contact, error) {
	uuids, err := h.repo.GetContacts()
	if err != nil {
		return nil, err
	}
	var contacts []*Contact
	for i := range uuids {
		c, err := h.GetContact(uuids[i])
		if err != nil {
			h.logger.WithField("ContactUUID", uuids[i]).WithError(err).Warnf("Error get contact from repo. ")
			continue
		}
		contacts = append(contacts, c)
	}
	return contacts, nil
}

func (h *contactHandler) AddContact(c Contact, pubKeys ...PubKeyData) error {
	if c.UUID == "" {
		return fmt.Errorf("Couldn't add Contaxct without uuid. ")
	}

	h.repo.AddContact(c)
	for i := range pubKeys {
		pubKeys[i].ContactUUID = c.UUID
		err := h.pubKeys.AddPubKey(pubKeys[i])
		if err != nil {
			h.logger.WithField("ContactUUID", c.UUID).WithField("PubKeyFingerprint", pubKeys[i].Fingerprint).WithError(err).Warnf("Error add pub key. ")
		}
	}

	return nil
}

func (h *contactHandler) AddPubKeysToContact(contactUUID string, pubKeys ...PubKeyData) error {
	h.lock.Lock()
	for i := range pubKeys {
		pubKeys[i].ContactUUID = contactUUID
		err := h.pubKeys.AddPubKey(pubKeys[i])
		if err != nil {
			h.logger.WithField("ContactUUID", contactUUID).WithField("PubKeyFingerprint", pubKeys[i].Fingerprint).WithError(err).Warnf("Error add pub key. ")
		}
	}
	return nil
}
