package domain

type PubKey interface {
	Fingerprint() string
	Validate(signature, hash []byte) error
	Encrypt([]byte) ([]byte, error)
}

type PubKeyParser func(PubKeyData) (PubKey, error)

type PubKeyData struct {
	Fingerprint string
	ContactUUID string
	Type        string
	Data        string
}
