package domain

import (
	"sync"
)

var (
	loggerOnce sync.Once
	logger     Logger
)

type Logger interface {
	Print(...interface{})
	Printf(string, ...interface{})
	Println(...interface{})

	Debug(...interface{})
	Debugf(string, ...interface{})
	Debugln(...interface{})

	Fatal(...interface{})
	Fatalf(string, ...interface{})
	Fatalln(...interface{})

	Panic(...interface{})
	Panicf(string, ...interface{})
	Panicln(...interface{})

	Warn(...interface{})
	Warnf(string, ...interface{})
	Warnln(...interface{})

	WithField(key string, value interface{}) Logger
	WithError(error) Logger
}

func (d *Domain) Logger() Logger {
	return d.logger
}
