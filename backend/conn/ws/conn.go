package conn

import (
	"fmt"
	"net/http"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/quarrck/quarrck/backend/domain"

	ws "github.com/gorilla/websocket"

	"github.com/vmihailenco/msgpack"
)

const (
	NotStarted = iota
	Init
	Connected
	Reconnecting
	Faild
	Closed = iota
)

type Package struct {
	From             string
	Fingerprint      string
	To               []string
	ConversationUUID string
	Type             string
	Content          []byte
	Signature        []byte
}

type conn struct {
	domain.Server
	user          domain.User
	status        uint32
	conn          *ws.Conn
	sendLock      sync.Mutex
	dialer        ws.Dialer
	conversations domain.ConversationHandler
	logger        domain.Logger
}

func Start(s domain.Server, user domain.User, conv domain.ConversationHandler, logger domain.Logger) (domain.Conn, error) {
	c := conn{
		Server:        s,
		user:          user,
		conversations: conv,
		logger:        logger,
	}
	return &c, c.Init()
}

func (c *conn) Connect() error {
	h := http.Header{}
	h.Add("Content-Type", "application/msgpack")
	h.Add("UUID", c.user.UUID())

	var err error
	c.conn, _, err = c.dialer.Dial(c.URL, h)
	if err != nil {
		return fmt.Errorf("Couldn't create websocket: %s, %v", c.URL, err)
	}
	return nil
}

func (c *conn) Init() error {
	atomic.StoreUint32(&c.status, Init)
	defer atomic.StoreUint32(&c.status, Faild)

	if err := c.Connect(); err != nil {
		c.logger.Warn(err)
	}

	go func() {
		for {
			_, r, err := c.conn.NextReader()
			if err != nil {
				c.logger.Warn(err)
				err = c.Connect()
				if err != nil {
					c.logger.Warn(err)
					time.Sleep(10 * time.Second)
				}
				continue
			}

			var pkg Package
			if err := msgpack.NewDecoder(r).Decode(&pkg); err != nil {
				c.logger.Warn(err)
				continue
			}

			if err := c.conversations.Receive(domain.Package{
				Version:          "1.0",
				From:             pkg.From,
				Fingerprint:      pkg.Fingerprint,
				ConversationUUID: pkg.ConversationUUID,
				Type:             domain.PackageType(pkg.Type),
				Content:          pkg.Content,
				Signatur:         pkg.Signature,
			}); err != nil {
				c.logger.WithError(err).WithField("Package", pkg).Warn("Error Receiving Package. ")
			}
		}
	}()

	return nil
}

func (c *conn) Send(dpkg domain.Package, to ...*domain.Contact) error {
	pkg := Package{
		From:             dpkg.From,
		Fingerprint:      dpkg.Fingerprint,
		ConversationUUID: dpkg.ConversationUUID,
		Type:             string(dpkg.Type),
		Signature:        dpkg.Signatur,
		Content:          dpkg.Content,
	}

	for i := range to {
		pkg.To = append(pkg.To, to[i].UUID)
	}

	c.sendLock.Lock()
	defer c.sendLock.Unlock()
	w, err := c.conn.NextWriter(ws.BinaryMessage)
	if err != nil {
		return fmt.Errorf("Couldn't send message: %v", err)
	}
	defer w.Close()
	return msgpack.NewEncoder(w).Encode(pkg)
}

func (c *conn) Status() string {
	return "running"
}

func (c *conn) Close() {
	atomic.StoreUint32(&c.status, Closed)
	c.conn.Close()
}

func (c *conn) Closed() bool {
	return atomic.LoadUint32(&c.status) == Closed
}
