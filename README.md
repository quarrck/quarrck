## Setup
```
    make setup
```

## Run
```
    make run -j2
```

## Build
```
    make build
```
