package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/google/uuid"

	_ "github.com/jinzhu/gorm/dialects/sqlite"

	frontend "gitlab.com/quarrck/quarrck/backend/frontend/astilectron"
	repo "gitlab.com/quarrck/quarrck/backend/repo/gorm"

	ws "gitlab.com/quarrck/quarrck/backend/conn/ws"

	"github.com/asticode/go-astikit"
	"github.com/asticode/go-astilectron"
	bootstrap "github.com/asticode/go-astilectron-bootstrap"
	log "github.com/sirupsen/logrus"
	"gitlab.com/quarrck/quarrck/backend/domain"
)

// Vars injected via ldflags by bundler

var (
	AppName            string = "quarrck"
	BuiltAt            string
	VersionAstilectron string
	VersionElectron    string
	Env                string = "dev"
)

var logger *log.Logger

func main() {
	dev := Env == "dev"
	logger = log.New()
	savedir, err := os.UserConfigDir()
	if err != nil {
		logger.Fatal(err)
	}

	savedir = filepath.Join(savedir, AppName)

	if dev {
		savedir = "./output/dev/"
	}

	if err := os.MkdirAll(savedir, 0777); err != nil {
		logger.Fatal(err)
	}

	if dev {
		logger.SetLevel(log.DebugLevel)
	} else {
		logfile, err := os.OpenFile(fmt.Sprintf(filepath.Join(savedir, fmt.Sprintf("%s.log", AppName))), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err != nil {
			log.Fatal(err)
		}

		defer logfile.Close()

		logger.SetOutput(logfile)

		logger.SetFormatter(&log.JSONFormatter{})
	}
	r, err := repo.Start("sqlite3", filepath.Join(savedir, fmt.Sprintf("%s.sqlite", AppName)))
	if err != nil {
		logger.Fatalf("Error start repo: %v", err)
	}

	id, err := r.GetConfig("UUID")
	if err != nil {
		if err := r.SaveConfig("UUID", uuid.New().String()); err != nil {
			logger.Fatal(err)
		}
	}

	u := user{
		uuid: id,
	}

	d, err := domain.New(u, &LogWrapper{logger.WithContext(context.Background())})
	if err := d.RegisterConfigRepo(r); err != nil {
		logger.Fatal(err)
	}
	var p pubKey
	if err := d.RegisterPubKeyRepo(p); err != nil {
		logger.Fatal(err)
	}
	if err := d.RegisterPubKeyParser("none", p.parse); err != nil {
		logger.Fatal(err)
	}
	if err := d.RegisterContactRepo(r); err != nil {
		logger.Fatal(err)
	}
	if err := d.RegisterServerRepo(r); err != nil {
		logger.Fatal(err)
	}
	if err := d.RegisterConnector("WS", ws.Start); err != nil {
		logger.Fatal(err)
	}
	if err := d.RegisterMessageRepo(r); err != nil {
		logger.Fatal(err)
	}
	if err := d.RegisterConversationRepo(r); err != nil {
		logger.Fatal(err)
	}

	if err := d.StartServer(); err != nil {
		logger.Fatalf("Error start server : %v", err)
	}

	f := frontend.New(d)

	aeOpts := astilectron.Options{
		AppName: AppName,
		//DataDirectoryPath:  "test",
		SingleInstance: true,
	}
	btOpts := bootstrap.Options{
		Debug:          dev,
		Logger:         logger,
		OnWait:         f.OnWait,
		IgnoredSignals: []os.Signal{},
	}

	win := bootstrap.Window{
		MessageHandler: f.HandleMessages,
		Options: &astilectron.WindowOptions{
			BackgroundColor: astikit.StrPtr("#333"),
			Center:          astikit.BoolPtr(true),
			Height:          astikit.IntPtr(700),
			Width:           astikit.IntPtr(700),
		},
	}

	if dev {
		win.Homepage = "http://localhost:8080"
	} else {
		btOpts.RestoreAssets = RestoreAssets
		btOpts.Asset = Asset
		btOpts.AssetDir = AssetDir
		aeOpts.AppIconDarwinPath = "resources/icon.icns"
		aeOpts.AppIconDefaultPath = "resources/icon.png"
		aeOpts.VersionAstilectron = VersionAstilectron
		aeOpts.VersionElectron = VersionElectron
		win.Homepage = "index.html"
	}

	btOpts.AstilectronOptions = aeOpts
	btOpts.Windows = []*bootstrap.Window{&win}

	if err := bootstrap.Run(btOpts); err != nil {
		logger.Fatal(fmt.Errorf("running bootstrap failed: %w", err))
	}
}

type eventError struct {
	Error error
}

type event struct {
	Event string
}

type pubKey struct{}

func (pubKey) Fingerprint() string {
	return "none"
}
func (pubKey) Validate([]byte, []byte) error {
	return nil
}
func (pubKey) Encrypt(b []byte) ([]byte, error) {
	return b, nil
}

func (pubKey) AddPubKey(domain.PubKeyData) error {
	return nil
}

func (pubKey) GetPubKey(s string) (domain.PubKeyData, error) {
	return domain.PubKeyData{
		Fingerprint: "none",
		Type:        "none",
	}, nil
}

func (pubKey) GetPubKeysByContact(contactUUID string) ([]string, error) {
	return []string{"none"}, nil
}

func (p pubKey) parse(domain.PubKeyData) (domain.PubKey, error) {
	return p, nil
}

type user struct {
	uuid string
}

func (u user) UUID() string {
	return u.uuid
}

func (user) Fingerprint() string {
	return "none"
}

func (user) Sign(b []byte) ([]byte, error) {
	return b, nil
}

func (user) Decrypt(b []byte) ([]byte, error) {
	return b, nil
}

type LogWrapper struct {
	*log.Entry
}

func (w *LogWrapper) WithField(key string, value interface{}) domain.Logger {
	return &LogWrapper{w.Entry.WithField(key, value)}
}

func (w *LogWrapper) WithError(err error) domain.Logger {
	return &LogWrapper{w.Entry.WithError(err)}
}
