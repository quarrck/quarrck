package main

import (
	"context"
	"fmt"
	"os"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	ws "gitlab.com/quarrck/quarrck/backend/conn/ws"

	"gitlab.com/quarrck/quarrck/backend/domain"
)

func main() {
	if len(os.Args) < 2 {
		logrus.Fatalf("Not enught Arguments")
	}
	id := "3ba8ebc6-d54c-4fde-bc76-9071f811a17b"
	convUUID := "c13b2ac4-85d3-45eb-ace9-8609330ba740"
	server := "ws://localhost:8888/connect"

	d, err := domain.New(user{
		uuid: id,
	}, &LogWrapper{Entry: logrus.WithContext(context.Background())})
	if err != nil {
		logrus.Fatal(err)
	}

	r := &Repo{
		UUID: id,
		Conversation: domain.ConversationData{
			UUID:   convUUID,
			Key:    "821bd32245ea13827f26e64e684ce64c407eb464bf9a5874494e7347ac370bf6",
			Status: "active",
		},
		Contact: domain.Contact{UUID: "5f63c2bd-67f7-4b4d-9630-006b3df7a18c"},
		Server: domain.Server{
			ID:   1,
			Type: "WS",
			URL:  server,
		},
	}

	if err := d.RegisterConfigRepo(r); err != nil {
		logrus.Fatal(err)
	}
	if err := d.RegisterPubKeyRepo(r); err != nil {
		logrus.Fatal(err)
	}
	if err := d.RegisterContactRepo(r); err != nil {
		logrus.Fatal(err)
	}
	if err := d.RegisterServerRepo(r); err != nil {
		logrus.Fatal(err)
	}
	if err := d.RegisterConnector("WS", ws.Start); err != nil {
		logrus.Fatal(err)
	}
	if err := d.RegisterMessageRepo(r); err != nil {
		logrus.Fatal(err)
	}
	if err := d.RegisterConversationRepo(r); err != nil {
		logrus.Fatal(err)
	}

	if err := d.StartServer(); err != nil {
		logrus.Fatalf("Error start server : %v", err)
	}

	conv, err := d.Conversation().GetConversation(convUUID)
	if err != nil {
		logrus.WithError(err).Fatalf("Error GetConversation(%s)", convUUID)
	}

	if err != nil {
		logrus.WithError(err).Fatalf("Error Start Server")
	}

	_, err = conv.SendMessage(domain.Message{
		UUID:       uuid.New().String(),
		AuthorUUID: id,
		Content:    os.Args[1],
	})
	if err != nil {
		logrus.WithError(err).Fatalf("Error Send Message")
	}
}

type LogWrapper struct {
	*logrus.Entry
}

func (w *LogWrapper) WithField(key string, value interface{}) domain.Logger {
	return &LogWrapper{w.Entry.WithField(key, value)}
}

func (w *LogWrapper) WithError(err error) domain.Logger {
	return &LogWrapper{w.Entry.WithError(err)}
}

type Repo struct {
	UUID         string
	Conversation domain.ConversationData
	Contact      domain.Contact
	Server       domain.Server
}

func (r *Repo) GetConfig(k string) (string, error) {

	switch k {
	case "UUID":
		return r.UUID, nil
	default:
		return "", fmt.Errorf("Not Implementet")
	}

}

func (r *Repo) SaveConfig(string, string) error {
	return fmt.Errorf("Not Implementet")
}

func (r *Repo) GetConversation(uuid string) (domain.ConversationData, error) {
	if r.Conversation.UUID != uuid {
		return domain.ConversationData{}, fmt.Errorf("Not Found! ")
	}
	return r.Conversation, nil
}

func (r *Repo) GetConversations(domain.GetConversationOpts) ([]string, error) {
	return []string{r.Conversation.UUID}, nil

}

func (r *Repo) AddConversation(domain.ConversationData, []string, []uint) error {
	panic("not implemented") // TODO: Implement
}

func (r *Repo) UpdateConversation(_ domain.ConversationData) error {
	panic("not implemented") // TODO: Implement
}

func (r Repo) GetConversationContacts(uuid string) ([]string, error) {
	if r.Conversation.UUID != uuid {
		return nil, fmt.Errorf("Not Found! ")
	}
	return []string{r.Contact.UUID}, nil
}

func (r Repo) GetConversationServers(uuid string) ([]uint, error) {
	if r.Conversation.UUID != uuid {
		return nil, fmt.Errorf("Not Found! ")
	}
	return []uint{r.Server.ID}, nil
}

func (r Repo) AddConversationContact(_ string, _ string) error {
	panic("not implemented") // TODO: Implement
}

func (r Repo) AddConversationServer(_ string, _ uint) error {
	panic("not implemented") // TODO: Implement
}

func (Repo) AddPubKey(domain.PubKeyData) error {
	panic("not implemented") // TODO: Implement
}

func (Repo) GetPubKey(string) (domain.PubKeyData, error) {
	panic("not implemented") // TODO: Implement
}

func (Repo) GetPubKeysByContact(string) ([]string, error) {
	return nil, nil
}

type user struct {
	uuid string
}

func (u user) UUID() string {

	return u.uuid
}
func (r Repo) GetContact(uuid string) (domain.Contact, error) {
	if r.Contact.UUID != uuid {
		return domain.Contact{}, fmt.Errorf("Not Found! ")
	}
	return r.Contact, nil
}

func (r Repo) GetContacts() ([]string, error) {
	return []string{r.Contact.UUID}, nil
}

func (r Repo) AddContact(_ domain.Contact) error {
	panic("not implemented") // TODO: Implement
}

func (r Repo) UpdateContact(_ *domain.Contact) error {
	panic("not implemented") // TODO: Implement
}

func (r Repo) GetServer(id uint) (domain.Server, error) {
	if r.Server.ID != id {
		return domain.Server{}, fmt.Errorf("Not Found! ")
	}
	return r.Server, nil
}

func (r Repo) GetServers() ([]domain.Server, error) {
	return []domain.Server{r.Server}, nil
}

func (r Repo) AddServer(_ domain.Server) (uint, error) {
	panic("not implemented") // TODO: Implement
}

func (r Repo) UpdateServer(_ domain.Server) error {
	panic("not implemented") // TODO: Implement
}

func (r Repo) GetMessages(_ domain.GetMessagesOpts) ([]domain.Message, error) {
	panic("not implemented") // TODO: Implement
}

func (r Repo) AddMessage(_ domain.Message) error {
	return nil
}

func (user) Fingerprint() string {
	return "none"
}

func (user) Sign(b []byte) ([]byte, error) {
	return b, nil
}

func (user) Decrypt(b []byte) ([]byte, error) {
	return b, nil
}
