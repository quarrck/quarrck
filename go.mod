module gitlab.com/quarrck/quarrck

go 1.15

require (
	github.com/AlexanderGrom/go-event v0.0.0-20180406111247-a2f1486e096e // indirect
	github.com/akavel/rsrc v0.10.1 // indirect
	github.com/asaskevich/EventBus v0.0.0-20200907212545-49d423059eef
	github.com/asticode/go-astikit v0.13.0
	github.com/asticode/go-astilectron v0.20.0
	github.com/asticode/go-astilectron-bootstrap v0.4.3
	github.com/asticode/go-astilectron-bundler v0.7.3 // indirect
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v1.14.4
	github.com/sirupsen/logrus v1.7.0
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	google.golang.org/appengine v1.6.7 // indirect
)
